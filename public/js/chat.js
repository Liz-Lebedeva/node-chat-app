const socket = io();

const params = {
    name: new URL(location.href).searchParams.get('userName').trim() || 'Anonymous',
    room: new URL(location.href).searchParams.get('roomName').trim()
};
const form = document.getElementById('message-form');
const messageTextbox = document.getElementById('message-text');
const sendMessage = document.getElementById('send-message');
const sendLocation = document.getElementById('send-location');
const chatHistory = document.getElementById('history');
const usersOnline = document.getElementById('list');
const messageTemplate = document.getElementById('message-template');
const locationTemplate = document.getElementById('location-template');


socket.on('connect', function() {
    console.log('Connected to the server');

    if (!params.room) {
        alert('Room name is required');
        window.location.href = '/';
    } else {
        socket.emit('connectedAs', params, function(err) {
            if (err) {
                alert(err);
                window.location.href = '/';
            }
        });
    }
});

socket.on('disconnect', function() {
    console.log('Disconnected from the server');
});

socket.on('updateUserList', function(list) {
    usersOnline.innerHTML = '';

    list.forEach( function(username) {
        const li = document.createElement('li');
        li.textContent = username;
        usersOnline.append(li);
    });
});

socket.on('newMessage', function(message) {
    const formattedTime = moment(message.createdAt).format('h:mm a');

    const template = messageTemplate.innerHTML;
    const li = document.createElement('li');
    li.innerHTML = Mustache.render(template, {
        from: message.from,
        text: message.text,
        timestamp: formattedTime
    });
    li.setAttribute('class', 'display-row');
    chatHistory.insertBefore(li, chatHistory.childNodes[0]);
});

socket.on('newLocationMessage', function(message) {
    const formattedTime = moment(message.createdAt).format('h:mm a');

    const template = locationTemplate.innerHTML;
    const li = document.createElement('li');
    li.innerHTML = Mustache.render(template, {
        from: message.from,
        url: message.url,
        timestamp: formattedTime
    });
    chatHistory.insertBefore(li, chatHistory.childNodes[0]);
});


form.addEventListener('submit', function(e) {
    e.preventDefault();

    const text = messageTextbox.value.trim();
    if (text) {
        sendMessage.setAttribute('disabled', 'disabled');
        sendMessage.textContent = 'Sending...';

        socket.emit('createMessage', text, function() {
            sendMessage.removeAttribute('disabled');
            sendMessage.textContent = 'Send Message';
        });
    }
    messageTextbox.value = null;
});

sendLocation.addEventListener('click', function(e) {
    if (!navigator.geolocation) {
        return alert('Geolocation not supported by your browser.');
    }
    sendLocation.setAttribute('disabled', 'disabled');
    sendLocation.textContent = 'Sending...';

    navigator.geolocation.getCurrentPosition( function(position) {
        socket.emit('shareLocation', {
            lat: position.coords.latitude,
            lon: position.coords.longitude
        }, function() {
            sendLocation.removeAttribute('disabled');
            sendLocation.textContent = 'Send Location';
        });
    }, function() {
        sendLocation.removeAttribute('disabled');
        sendLocation.textContent = 'Send Location';
        return alert('Unable to fetch location.');
    });
});