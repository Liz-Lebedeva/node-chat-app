# Chat Application

This is a real-time chat application built with socket.io that I did as a part of [The Complete Node.js Developer Course (2nd Edition)](https://www.udemy.com/the-complete-nodejs-developer-course-2/learn/v4/overview).  
Chat applicaton allows users to send text messages and geo location links to other users within the same room. Users in other rooms will not see the messages/links sent by other users as well as their online presence.

Geo location feature is build with HTML Geolocation API (supported by all major browsers) and Google Maps API.





## Deployment notes

Chat Application is currently deployed [on Heroku](https://rocky-depths-70533.herokuapp.com/)


## Built With

* [express](http://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [socket.io](https://socket.io/) - Real-time bidirectional event-based
* [moment](https://momentjs.com/) - Lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates
* [mustache js](https://mustache.github.io/) - Logic-less template syntax
* [mocha](https://mochajs.org/) - Test framework
* [chai](http://www.chaijs.com/) - Assertion library