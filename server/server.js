const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000;

const {generateMessage, generateLocationMessage} = require('./utils/message');
const {isRealString} = require('./utils/validation');
const {Users} = require('./utils/users');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);
const users = new Users();

app.use(express.static(publicPath));

io.on('connection', (socket) => {
    console.log('New user connected');

    socket.on('connectedAs', (user, callback) => {
        if (!isRealString(user.name) || !isRealString(user.room)) {
            return callback('Room name is required')
        }

        socket.join(user.room);
        users.removeUser(socket.id); // removes user from previously joined rooms and adds to the last selected room
        users.addUser(user.room, user.name, socket.id);

        io.to(user.room).emit('updateUserList', users.getUserList(user.room));
        socket.emit('newMessage', generateMessage(`Welcome ${user.name}!`));
        socket.broadcast.to(user.room).emit('newMessage', generateMessage(`${user.name} has joined`));

        callback();
    });

    socket.on('createMessage', (message, callback) => {
        const user = users.getUser(socket.id);
        if (user && isRealString(message)) {
            io.to(user.room).emit('newMessage', generateMessage(message, user.name));
        }
        callback();
    });

    socket.on('shareLocation', (coords, callback) => {
        const user = users.getUser(socket.id);
        if (user) {
            io.to(user.room).emit('newLocationMessage', generateLocationMessage(coords.lat, coords.lon, user.name));
        }
        callback();
    });

    socket.on('disconnect', () => {
        console.log('User disconnected');

        const disconnectedUser = users.removeUser(socket.id);
        if (disconnectedUser) {
            io.to(disconnectedUser.room).emit('updateUserList', users.getUserList(disconnectedUser.room));
            io.to(disconnectedUser.room).emit('newMessage', generateMessage(`${disconnectedUser.name} has left`));
        }
    });
});


server.listen(port, () => {
    console.log(`Started on port ${port}`);
});