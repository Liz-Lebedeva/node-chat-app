const expect = require('chai').expect;

const {generateMessage, generateLocationMessage} = require('./message');


describe('generateMessage', () => {

    it('should generate message with correct author and text plus a time stamp', () => {
        // Define test data
        const from = 'Ann';
        const text = 'Test message 12345';

        const message = generateMessage(text, from);

        expect(message.from).to.equal(from);
        expect(message.text).to.equal(text);
        expect(message.createdAt).to.be.a('number');
    });

    it('should set from as Admin if it is not defined', () => {
        // Define test data
        const text = 'Test message 12345';

        const message = generateMessage(text);

        expect(message.from).to.equal('Admin');
        expect(message.text).to.equal(text);
        expect(message.createdAt).to.be.a('number');
    });

    it('should generate geolocation message link with correct coordinates and a timestamp', () => {
        // Define test data
        const from = 'Ann';
        const lat = '43.706777599999995';
        const lon = '-79.3919488';

        const message = generateLocationMessage(lat, lon, from);

        expect(message.from).to.equal(from);
        expect(message.url).to.include(`?q=${lat},${lon}`);
        expect(message.createdAt).to.be.a('number');
    });

});