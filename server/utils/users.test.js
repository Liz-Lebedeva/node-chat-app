const expect = require('chai').expect;

const {Users} = require('./users');

describe('Users', () => {
    let users;

    beforeEach( () => {
        // Create and populate Users array
        users = new Users();

        users.list = [{
            id: '101',
            name:'User 1',
            room: 'Room 1'
        }, {
            id: '102',
            name:'User 2',
            room: 'Room 1'
        }, {
            id: '103',
            name:'User 3',
            room: 'Room 2'
        }, {
            id: '104',
            name:'User 4',
            room: 'Room 2'
        }];

    });

    it('addUser should generate user from usermane and room', () => {
        // Define test data
        const username = 'Test User';
        const room = 'Test Room';

        const resUser = users.addUser(room, username);

        expect(resUser.name).to.equal(username);
        expect(resUser.room).to.equal(room);
        expect(resUser.id).to.be.a('String');
    });

    it('addUser should add user to the beginning of the list array', () => {
        // Define test data
        const username = 'test user';
        const room = 'test room';
        const initListLength = users.list.length;

        users.addUser(room, username);

        expect(users.list.length).to.equal(initListLength + 1);
        expect(users.list[0].name).to.equal(username);
        expect(users.list[0].room).to.equal(room);
        expect(users.list[0].id).to.be.a('String');
    });

    it('getUserList should return array of names', () => {
        // Define test data
        const room = 'Room 1';

        const result = users.getUserList(room);

        expect(result.length).to.equal(2);
        expect(result).to.include('User 1', 'User 2');
    });

    it('getUser should return a user by id', () => {
        // Define test data
        const user = users.list[1];

        result = users.getUser(user.id);

        expect(result).to.equal(user);

    });

    it('removeUser should remove user from the array', () => {
        // Define test data
        const user = users.list[1];
        const initListLength = users.list.length;

        users.removeUser(user.id);

        expect(users.list.length).to.equal(initListLength - 1);
        expect(users).to.not.include(user);

    });

    it('removeUser should not remove user if id not found', () => {
        // Define test data
        const id = '584';
        const initListLength = users.list.length;

        users.removeUser(id);

        expect(users.list.length).to.equal(initListLength);
    });

});