class Users {

    constructor() {
        this.list = [];
    }

    addUser(room, name, id) {
        const user = {
            room,
            name,
            id: id || Math.random().toString(36).substring(2, 15) // random id for testing purposes
        };
        // add user to the beginning of the array to display new users first
        this.list.unshift(user);
        return user;
    };

    removeUser(id) {
        const user = this.getUser(id);
        if (user) {
            this.list.splice( this.list.indexOf(user),1 );
        }
        return user;
    }

    getUser(id) {
        const user = this.list.filter( (user) => user.id === id );
        return user[0];
    }

    getUserList(room) {
        const list = this.list.filter( (user) => user.room === room );
        return list.map( (user) => user.name);
    }
}

module.exports = {Users};