const moment = require('moment');

const generateMessage = (text, from) => {
    return {
        from: from || 'Admin',
        text,
        createdAt: moment().valueOf()
  }
};

const generateLocationMessage = (lat, lon, from) => {
    return {
        from,
        url: `https://google.com/maps?q=${lat},${lon}`,
        createdAt: moment().valueOf()
    }
};

// https://google.com/maps?q=43.706777599999995,-79.3919488

module.exports = {generateMessage, generateLocationMessage};